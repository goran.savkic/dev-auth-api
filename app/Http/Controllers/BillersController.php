<?php

namespace App\Http\Controllers;

class BillersController extends Controller
{
    private $billers = [
        'EPOCH' => 'Epoch',
        'SEGPAY' => 'Segpay',
        'CCBILL' => 'CCBill',
        'RGNATIVE' => 'RGNATIVE',
        'WTS' => 'WTS',
        'GXB' => 'GXB',
        'NETBILLING' => 'Netbilling',
        'LOCALB' => 'Localb',
    ];

    /**
     * \App\PaperStreetMedia\Upsell\Upgrade::getBillerName
     * @param $biller_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBillerInfo($biller_id)
    {
        $response = new \stdClass();
        $response->data = ['biller' => $biller_id];
        return response()->json($response);
    }

    public function getBillerDetails($biller_id)
    {
        $response = new \stdClass();
        $response->data = [];
        return response()->json($response);
    }
}
