<?php

namespace App\Http\Controllers;

class MemberController extends Controller
{
    private $users = [
        'jshussain' => [
                'info' => [
                    [
                        'status' => 1,
                        'siteID' => 15,
                        'password' => 'q1w2e3r4',
                        'username' => 'jshussain',
                        'memberID' => 1,
                        'emailAddress' => 'jshussain@gmail.com',
                        'dateJoined' => '2018-01-01',
                        'isTrial' => 0,
                    ]
                ],
                'details' => [
                        [
                            'siteid' => 15,
                            'status' => 1,
                            'memberid' => 1,
                            'billerid' => 'SEGPAY',
                            'trial' => 0,
                            'password' => 'q1w2e3r4',
                            'firstname' => 'jshussain',
                            'lastname' => 'xxx',
                            'email' => 'jshussain@gmail.com',
                            'memberidx' => 1,
                            'optionid' => 1,
                        ]
                ],
            ],
        'bigdpervy' => [
            'info' => [
                [
                    'status' => 2, // DISABLED USER
                    'siteID' => 15,
                    'password' => 'q1w2e3r4',
                    'username' => 'bigdpervy',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                ]
            ],
            'details' => [
                [
                    'siteid' => 15,
                    'status' => 2,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bigdpervy',
                    'lastname' => 'xxx',
                    'email' => 'jbigdpervy@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ]
            ],
        ],
        'bvguser' => [
            'info' => [
                [
                    'status' => 1,
                    'siteID' => 85,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvguser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                ]
            ],
            'details' => [
                [
                    'siteid' => 85,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvguser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ]
            ],
        ],
        'bvgtsuser' => [
            'info' => [
                [
                    'status' => 1,
                    'siteID' => 15,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvgtsuser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ],
                [
                    'status' => 1,
                    'siteID' => 85,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvgtsuser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ]
            ],
            'details' => [
                [
                    'siteid' => 15,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvgtsuser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ],
                [
                    'siteid' => 85,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvgtsuser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ]
            ],
        ],
        'tsuser' => [
            'info' => [
                [
                    'status' => 1,
                    'siteID' => 15,
                    'password' => 'q1w2e3r4',
                    'username' => 'tsuser',
                    'memberID' => 1,
                    'emailAddress' => 'tsuser@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ]
            ],
            'details' => [
                [
                    'siteid' => 15,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'tsuser',
                    'lastname' => 'xxx',
                    'email' => 'tsuser@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ]
            ],
        ],
        'bvgslmtsuser' => [
            'info' => [
                [
                    'status' => 1,
                    'siteID' => 15,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvgslmtsuser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ],
                [
                    'status' => 1,
                    'siteID' => 85,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvgslmtsuser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ],
                [
                    'status' => 1,
                    'siteID' => 74,
                    'password' => 'q1w2e3r4',
                    'username' => 'bvgslmtsuser',
                    'memberID' => 1,
                    'emailAddress' => 'bigdpervy@gmail.com',
                    'dateJoined' => '2018-01-01',
                    'isTrial' => 0,
                ]
            ],
            'details' => [
                [
                    'siteid' => 15,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvgslmtsuser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ],
                [
                    'siteid' => 85,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvgslmtsuser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ],
                [
                    'siteid' => 74,
                    'status' => 1,
                    'memberid' => 1,
                    'billerid' => 'SEGPAY',
                    'trial' => 0,
                    'password' => 'q1w2e3r4',
                    'firstname' => 'bvgslmtsuser',
                    'lastname' => 'xxx',
                    'email' => 'djole123@gmail.com',
                    'memberidx' => 1,
                    'optionid' => 1,
                ]
            ],
        ],

    ];

    public function getAuth()
    {
        $response = new \stdClass();
        $response->data = ['success' => true];
        return response()->json($response);
    }

    public function getSitesUser($username)
    {
        $response = new \stdClass();
        $response->data = $this->users[$username]['info'];

        return response()->json($response);
    }

    public function getUserInfo($username)
    {
        $response = new \stdClass();
        $response->data = $this->users[$username]['details'];
        return response()->json($response);
    }

    public function getMemberBillerInfo()
    {
        $response = new \stdClass();
        $response->data = [];
        return response()->json($response);
    }

    public function getMemberByNatsId()
    {
        $response = new \stdClass();
        $response->memberidx = '15:85';
        return response()->json($response);
    }

    public function getMemberByUsername()
    {
        $response = new \stdClass();
        $response->data = [];
        return response()->json($response);
    }

    public function getMemberUserInfo()
    {
        $response = new \stdClass();
        $response->data = [];
        return response()->json($response);
    }

}
