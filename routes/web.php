<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('sites/{site_id}', ['uses' => 'SitesController@getSiteInfo']);

$router->group(['prefix' => 'member'], function() use ($router) {
    $router->post('auth', ['uses' => 'MemberController@getAuth']);
    $router->get('sites/all/{username}', ['uses' => 'MemberController@getSitesUser']);
    $router->get('{username}/info', ['uses' => 'MemberController@getUserInfo']);
    $router->get('_memberidx_bynatsid.json', ['uses' => 'MemberController@getMemberByNatsId']);
    $router->get('_byusername.json', ['uses' => 'MemberController@getMemberByUsername']);
    $router->get('_member_userinfo.json', ['uses' => 'MemberController@getMemberUserInfo']);
});

$router->get('members-biller-info', ['uses' => 'MemberController@getMemberBillerInfo']);

$router->get('billers/{biller_id}', ['uses' => 'BillersController@getBillerInfo']);

$router->get('biller-options/{biller_id}/details', ['uses' => 'BillersController@getBillerDetails']);